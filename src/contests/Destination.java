package contests;

import java.util.Scanner;

public class Destination {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();
        for (int i = 0; i < t; i++) {
            int n = scanner.nextInt(), d = scanner.nextInt();
            int count = 0;
            for (int j = 0; j < n; j++) {
                int v = scanner.nextInt(), f = scanner.nextInt(), c = scanner.nextInt();
                if (v * f >= d * c) count++;
            }
            System.out.println(count);
        }
    }
}