package contests;

import java.util.Locale;
import java.util.Scanner;

public class PerimeterAndAreaOfATriangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        double aX = scanner.nextDouble(), aY = scanner.nextDouble(), bX = scanner.nextDouble(), bY = scanner.nextDouble(),
                cX = scanner.nextDouble(), cY = scanner.nextDouble();

        double firstLine, secondLine, thirdLine;
        firstLine = Math.sqrt(Math.pow(aX - bX, 2) + Math.pow(aY - bY, 2));
        secondLine = Math.sqrt(Math.pow(aX - cX, 2) + Math.pow(aY - cY, 2));
        thirdLine = Math.sqrt(Math.pow(cX - bX, 2) + Math.pow(cY - bY, 2));

        double perimeter, area;
        perimeter = firstLine + secondLine + thirdLine;
        double halfPerimeter = perimeter / 2;
        area = Math.sqrt(halfPerimeter * (halfPerimeter - firstLine) * (halfPerimeter - secondLine) * (halfPerimeter - thirdLine));
        System.out.format(Locale.US, "%.4f", getAnswer(perimeter));
        System.out.print(" ");
        System.out.format(Locale.US, "%.4f", getAnswer(area));
    }

    public static double getAnswer(double perimeter) {
        perimeter = Math.round(perimeter * 10000);
        perimeter = perimeter / 10000;
        return perimeter;
    }
}
