package contests;

import java.util.Scanner;

public class Angle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int ch = scanner.nextInt();
            int min = scanner.nextInt();
            if (ch == 0 && min == 0) {
                break;
            } else {
                int dif = Math.abs((ch * 5) - min);
                double grad;
                if (ch * 5 > min)
                    grad = ((6 * dif) + (0.5 * min));
                else
                    grad = ((6 * dif) - (0.5 * min));
                if (grad > 180) grad = 360.0 - grad;
                String s;
                if (min == 0) s = "00";
                else if (min < 10 && min > 0) s = "0" + min;
                else s = String.valueOf(min);
                System.out.println("At " + ch + ":" + s + " the angle is " + Math.abs(grad) + " degrees.");
            }
        }
    }
}
