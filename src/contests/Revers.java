package contests;

import java.util.Scanner;

public class Revers {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        StringBuilder stringBuilder = new StringBuilder(scanner.nextLine());
        int a = scanner.nextInt(), b = scanner.nextInt();

        StringBuilder str = new StringBuilder(stringBuilder.substring(a - 1, b));
        str.reverse();

        System.out.println(stringBuilder.replace(a - 1, b, str.toString()));

    }
}