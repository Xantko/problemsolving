package contests;

import java.util.Locale;
import java.util.Scanner;

public class LengthOfTheVector {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double xOne = scanner.nextDouble(), yOne = scanner.nextDouble(),
                xTwo = scanner.nextDouble(), yTwo = scanner.nextDouble();
        double vector = Math.sqrt(Math.pow(xOne - xTwo, 2) + Math.pow(yOne - yTwo, 2));
        System.out.format(Locale.ENGLISH, "%.6f", answer(vector));
    }

    public static double answer(double vector) {
        vector = Math.round(vector * 1000000);
        vector = vector / 1000000;
        return vector;
    }
}
