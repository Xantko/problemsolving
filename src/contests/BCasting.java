package contests;

import java.util.Scanner;

public class BCasting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int test, b;
        String d;
        int t = scanner.nextInt();
        for (int i = 0; i < t; i++) {
            long number = 0;
            test = scanner.nextInt();
            b = scanner.nextInt();
            d = scanner.next();
            int pow = d.length() - 1;
            for (int j = 0; j < d.length(); j++) {
                number += Character.getNumericValue(d.charAt(j)) * Math.pow(b, pow);
                pow--;
            }
            //System.out.println(number);
            System.out.println(test + " " + number % (b - 1));
        }
    }
}
