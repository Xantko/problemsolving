package contests;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Arithmetic {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        List<String> list = new ArrayList<>();
        String count = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != '*' && s.charAt(i) != '+') {
                count += s.charAt(i);
            } else if (s.charAt(i) == '*' || s.charAt(i) == '+') {
                list.add(count);
                count = "";
                list.add(String.valueOf(s.charAt(i)));
            }
        }
        list.add(count);
        int answer = Integer.parseInt(list.get(0));
        if (list.size() > 1) {
            for (int i = 1; i < list.size(); i += 2) {
                if (list.get(i).equals("+")) answer += Integer.parseInt(list.get(i + 1));
                if (list.get(i).equals("*")) answer *= Integer.parseInt(list.get(i + 1));
            }
        }
        System.out.println(answer);
    }
}
