package contests;

import java.util.Scanner;

public class TheHappyTourist {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int l, p, v;
        int res;
        int i = 1;
        while (true) {
            l = scanner.nextInt();
            p = scanner.nextInt();
            v = scanner.nextInt();
            if (l != 0 && p != 0 && v != 0) {
                res = (l * (v / p));
                if (v % p <= l) res += v % p;
                else res += l;
                System.out.println("Case " + i + ": " + res);
                i++;
            } else {
                break;
            }

        }
    }
}
