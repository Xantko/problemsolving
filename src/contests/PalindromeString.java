package contests;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PalindromeString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        List<Character> list = new ArrayList();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' ') list.add(s.charAt(i));
        }

        int secondChar = list.size() - 1;
        boolean check = true;
        for (int i = 0; i < list.size() / 2; i++) {
            if (list.get(i) == list.get(secondChar)) secondChar--;
            else {
                check = false;
                break;
            }
        }
        if (check) System.out.println("YES");
        else System.out.println("NO");
    }
}
