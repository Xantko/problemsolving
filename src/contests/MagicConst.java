package contests;

import java.util.Scanner;

public class MagicConst {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextInt();
        long s = (int) Math.sqrt(n * 2);
        long d;
        boolean t = false;
        for (long i = s - 3; i < s + 3; i++) {
            d = (i * (i + 1)) / 2;
            if (d == n && n != 1) {
                System.out.println(i);
                t = true;
                break;
            }
        }
        if (t == false) System.out.println(-1);
    }
}
