package contests;

import java.util.Scanner;

public class Toasties {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        int n = 0;
        int k;
        String temp = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' ') temp += s.charAt(i);
            else {
                n = Integer.parseInt(temp);
                temp = "";
            }
        }
        k = Integer.parseInt(temp);
        if (n < k && n != 0) System.out.println(4);
        else if (n % k == 0) System.out.println(n / k * 4);
        else if (n % k > (double) k / 2) {
            while (n % k != 0) {
                n++;
            }
            System.out.println(n / k * 4);
        } else if (n % k <= (double) k / 2) {
            while (n % k != 0) {
                n++;
            }
            System.out.println(n / k * 4 - 2);
        }
    }
}
