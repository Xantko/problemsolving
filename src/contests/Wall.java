package contests;

import java.util.Scanner;

public class Wall {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt(), m = scanner.nextInt(), k = scanner.nextInt();

        int per = n * m;
        int count = 0;
        int ost = 0;
        int temp = k;
        int number = 1;
        while (count < per) {
            if (temp >= n) {
                temp -= n;
                count += n;
            } else if (temp < n) {
                ost += temp;
                temp = k;
                number++;
            }
        }
        if (temp != 20) ost += temp;
        System.out.println(number + " " + ost);
    }
}
