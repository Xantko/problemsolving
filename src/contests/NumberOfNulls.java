package contests;

import java.util.Scanner;

public class NumberOfNulls {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        int count = 0;
        int step = 1;
        while (true) {
            double a = n / Math.pow(5, step);
            if (a > 0) {
                count += a;
                step++;
            } else {
                break;
            }
        }
        System.out.println(count);
    }
}
