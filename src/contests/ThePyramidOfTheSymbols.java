package contests;

import java.util.Scanner;

public class ThePyramidOfTheSymbols {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char symbol = scanner.next().charAt(0);
        int number = scanner.nextInt();

        System.out.println(((number + (number * 2 - 1)) * number) / 2);
        int count;
        for (int i = 1; i <= number; i++) {
            count = number + i - 1;
            int numberOfSymbols = i * 2 - 1;
            int spaces = count - numberOfSymbols;
            while (true) {
                if (spaces > 0) {
                    System.out.print(' ');
                    spaces--;
                    count--;
                } else if (spaces == 0 && count > 0){
                    System.out.print(symbol);
                    count--;
                }else {
                    System.out.println();;
                    break;
                }
            }
        }
    }
}
