package contests;

import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        int firstLine = 0;
        int secondLine = s.length() - 1;
        boolean check = true;
        for (int i = 0; i < s.length() / 2; i++) {
            if (s.charAt(firstLine) == s.charAt(secondLine)) {
                firstLine++;
                secondLine--;
            } else {
                check = false;
                break;
            }
        }
        if (check) System.out.println("Yes");
        else System.out.println("No");
    }
}

