package contests;

import java.util.Locale;
import java.util.Scanner;

public class TheFormulaOfGeron {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        double a = scanner.nextDouble(), b = scanner.nextDouble(), c = scanner.nextDouble(), d = scanner.nextDouble(),
                f = scanner.nextDouble();
        double firstHalfPerimeter = (a + b + f) / 2;
        double secondHalfPerimeter = (c + d + f) / 2;
        double firstArea = Math.sqrt(firstHalfPerimeter * (firstHalfPerimeter - a) * (firstHalfPerimeter - b) *
                (firstHalfPerimeter - f));
        double secondArea = Math.sqrt(secondHalfPerimeter * (secondHalfPerimeter - c) * (secondHalfPerimeter - d) *
                (secondHalfPerimeter - f));
        double commonArea = firstArea + secondArea;
        commonArea = Math.round(commonArea * 10000);
        commonArea = commonArea / 10000;
        System.out.format(Locale.US, "%.4f", commonArea);
    }
}
