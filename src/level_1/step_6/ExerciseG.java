package level_1.step_6;

import java.util.Scanner;

public class ExerciseG {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s1 = scanner.nextLine();
        String out = "";
        String rev = "";
        for (int i = 0; i < s1.length(); ) {
            char j = s1.charAt(i);
            if (j != '(' && j != ')') {
                out += j;
                i++;
            } else if (j == '(') {
                i++;
                j = s1.charAt(i);
                rev = "";
                while (j != ')') {
                    rev += j;
                    i++;
                    j = s1.charAt(i);
                }
                out += revers(rev);
                i++;
            }
        }
        System.out.println(out);
    }

    public static String revers(String s) {
        String answer = "";
        for (int i = s.length() - 1; i >= 0; i--) {
            answer += s.charAt(i);
        }
        return answer;
    }
}
