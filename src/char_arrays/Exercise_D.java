package char_arrays;

import java.util.Scanner;

public class Exercise_D {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        char[] chars = s.toCharArray();
        boolean check = false;
        int count = 0;
        for (int i = 0; i < chars.length; i++){
            if (chars[i] == ' ' && check) count++;
            if (chars[i] == ' ') check = false;
            else check = true;
        }
        if (chars[chars.length - 1] != ' ') count++;
        System.out.println(count);
    }
}
