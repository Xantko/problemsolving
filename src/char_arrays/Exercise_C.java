package char_arrays;

import java.util.Scanner;

public class Exercise_C {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        char[] chars = new char[250];
        int count = 0;
        for (int i = 0; i < s.length(); i++){
            chars[i] = s.charAt(i);
        }
        for (int i = 1; i < chars.length; i++){
            if (chars[i] == '+' || chars[i] == '*' || chars[i] == '-')
                count++;
        }
        System.out.println(count);
    }
}
