package char_arrays;

import java.util.Scanner;

public class Exercise_F {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        char first;
        if (s.charAt(0) != '-')  first = s.charAt(0);
        else first = s.charAt(1);
        char last = s.charAt(s.length() - 1);
        int answer = Character.getNumericValue(first) + Character.getNumericValue(last);
        System.out.println(answer);
    }
}
